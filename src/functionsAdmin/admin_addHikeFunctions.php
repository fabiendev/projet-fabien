<?php
require_once('../includes/_dispacher.php');

// Ajouter une randonnée en BDD

function addHike() {
    global $db;
    extract($_POST);
    $validation = true;
    $erreur = [];
    
    if (empty($title) || empty($introduction) || empty($description) || empty($area) || empty($city) || empty($id_level) || empty($duration)) {
        $validation = false;
        $erreur[] = 'Tous les champs requis sont obligatoires.';
    }

    if(!isset($_FILES['picture']) OR $_FILES['picture']['error'] > 0) {
        $validation = false;
        $erreurs[] = 'Merci de charger un fichier.';
    }

    if (!empty($FILES['picture']['type']) && $_FILES['picture']['type'] != 'image/jpg' && $_FILES['picture']['type'] != 'image/jpeg' && $_FILES['picture']['type'] != 'image/png') {
        $validation = false;
        $erreur[] = 'Merci de charger un fichier avec l\'une de ces extensions : jpg, jpeg, png';
    }
    
    if ($validation) {
        
        move_uploaded_file($_FILES['picture']['tmp_name'], '../assets/image/hiking/' . basename($_FILES['picture']['name']));

		$data = [
			'title' => $_POST['title'],
            'introduction' => $_POST['introduction'],
            'description' => $_POST['description'],
			'information' => $_POST['information'],
			'area' => $_POST['area'],
			'city' => $_POST['city'],
			'id_level' => $_POST['id_level'],
			'elevation' => $_POST['elevation'],
			'duration' => $_POST['duration'],
            'distance' => $_POST['distance'],
            'picture' => basename($_FILES["picture"]["name"]),
			'id_user' => $_POST['id_user']
		];
        $sql = 'INSERT INTO hikes (title, introduction, description, information, area, city, id_level, elevation, duration, distance, picture, id_user) 
        VALUES (:title, :introduction, :description, :information, :area, :city, :id_level, :elevation, :duration, :distance, :picture, :id_user)';
        $request = $db->prepare($sql);
        $request->execute($data);

        unset($_POST);
    }
    
    return $erreur;
}