<?php
require_once('../includes/_dispacher.php');

// Récupére le titre de la randonnée depuis la BDD

function getCurrentHike() {
	global $db;
	$data['id'] = $_GET['id'];
	$sql = 'SELECT title, id_user FROM hikes WHERE id = :id';
	$request = $db->prepare($sql);
	$request->execute($data);
	$results = $request->fetch();

	return ($results) ? $results : [];
}

$currentHike = getCurrentHike();

// Vérifier si la randonnée existe en BDD

function existHike($currentHike) {
	if (empty($currentHike)) {
		header('Location: ' . 'admin_index.php'); 
		die();
    }
}

existHike($currentHike);

// Si l'utilisateur n'a pas le rôle 'admin' vérifie que la randonnée lui appartient

$currentUserRole = $_SESSION['role']; 

function isMyHike($currentHike, $currentUserRole) {
    if ($currentUserRole !== 'admin' && $_SESSION['id'] !== $currentHike['id_user']) {
        header('Location: ' . 'admin_index.php');
    }
}

isMyHike($currentHike, $currentUserRole);


// Fonction pour supprimer une randonnée après confirmation

function delete() {
	global $db;

	if (!empty($_GET['confirm'])) {
		$data['id'] = $_GET['id'];
		$sql = 'DELETE FROM hikes WHERE id = :id';
		$request = $db->prepare($sql);
		$request->execute($data);

		notif('La randonnée a bien été supprimée.', 'success');
	
		header('Location: ' . 'admin_index.php');
		die();
    }
}

delete();
