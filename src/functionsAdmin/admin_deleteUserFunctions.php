<?php
require_once('../includes/_dispacher.php');

// Vérifie le rôle de l'utilisateur et le renvoi vers l'index s'il n'est pas admin
checkRole();

// Récupérer le pseudo de l'utilisateur depuis la BDD

function getCurrentUser() {
	global $db;
	$data['id'] = $_GET['id'];
	$sql = 'SELECT username FROM users WHERE id = :id';
	$request = $db->prepare($sql);
	$request->execute($data);
	$results = $request->fetch();

	return ($results) ? $results : [];
}

$currentUser = getCurrentUser();

// Vérifier si l'utilisateur existe en BDD

function existUser($currentUser) {
	if (empty($currentUser)) {
		header('Location: ' . 'admin_list-users.php'); 
		die();
    }
}

existUser($currentUser);


// Fonction pour supprimer un utilisateur après confirmation

function delete() {
	global $db;

	if (!empty($_GET['confirm'])) :
		$data['id'] = $_GET['id'];
		$sql = 'DELETE FROM users WHERE id = :id';
		$request = $db->prepare($sql);
		$request->execute($data);

		notif('L\'utilisateur a bien été supprimé.', 'success');
	
		header('Location: ' . 'admin_list-users.php');
		die();
	endif;
}

delete();
