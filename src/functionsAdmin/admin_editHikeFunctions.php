<?php
require_once('../includes/_dispacher.php');

// Vérifie l'url et renvoie vers la page Liste utilisateur (ou admin_index si pas admin) si ID est vide ou n'est pas créé (fonction sur la page tools.php)
validGetId();


// Vérifie si l'ID de $_GET correspond à une randonnée dans la BDD et récupère les infos

function existHike() {
	$result = [];
	if (!empty($_GET['id'])) { 
		global $db;
		$data['id'] = $_GET['id'];
		$sql = 'SELECT * FROM hikes WHERE id = :id';
		$request = $db->prepare($sql);
		$request->execute($data);
		$result = $request->fetch();
		
		if (empty($result)) {
			header('Location: ' . 'admin_list-hikes.php');
			die();
		}
	}
	return $result;
}

$hikeInfo = existHike();

// Récupère le nom du niveau de la randonnée courante

function getHikeLevel($hikeInfo) {
    global $db;
    $data['id'] = $hikeInfo['id_level'];
    $sql = 'SELECT id, name FROM levels 
    WHERE id = :id';
    $request = $db->prepare($sql);
    $request->execute($data);
	return $request->fetch();
}

$hikeLevel = getHikeLevel($hikeInfo);

// Si l'utilisateur n'a pas le rôle 'admin' vérifie que la randonnée lui appartient

$currentUserRole = $_SESSION['role']; 

function isMyHike($hikeInfo, $currentUserRole) {
    if ($currentUserRole !== 'admin' && $_SESSION['id'] !== $hikeInfo['id_user']) {
        header('Location: ' . 'admin_index.php');
    }
}

isMyHike($hikeInfo, $currentUserRole);

// Met à jour les infos d'une randonnée en BDD

function updateHike() {
    global $db;

    extract($_POST);
    
    $validation = true;
    $erreur = [];
    
    if (empty($title) || empty($introduction) || empty($description) ||  empty($city) ||  empty($duration)) {
        $validation = false;
        $erreur[] = 'Vous ne pouvez pas supprimer ces champs';
    }

    if (!empty($FILES['picture']['type']) && $_FILES['picture']['type'] != 'image/jpg' && $_FILES['picture']['type'] != 'image/jpeg' && $_FILES['picture']['type'] != 'image/png') {
        $validation = false;
        $erreur[] = 'Merci de charger un fichier avec l\'une de ces extensions : jpg, jpeg, png';
    }

    if ($validation) {
    
        $data = [
            'title' => $_POST['title'],
            'introduction' => $_POST['introduction'],
            'description' => $_POST['description'],
			'information' => $_POST['information'],
			'area' => $_POST['area'],
			'city' => $_POST['city'],
			'id_level' => $_POST['id_level'],
			'elevation' => $_POST['elevation'],
			'duration' => $_POST['duration'],
            'distance' => $_POST['distance'],
            'id' => $_GET['id']
        ];

        $picture = '';
        if (!empty($_FILES['picture']['name'])) {
            move_uploaded_file($_FILES['picture']['tmp_name'], '../assets/image/user/' . basename($_FILES['picture']['name']));

            $data['picture'] = basename($_FILES['picture']['name']);
            $picture = ' picture = :picture, ';
        }

        $sql = 'UPDATE hikes SET title = :title, introduction = :introduction, description = :description, information = :information, area = :area, city = :city, id_level = :id_level,  elevation = :elevation, duration = :duration, ' . $picture .' distance = :distance  
        WHERE id = :id';
        $request = $db->prepare($sql);
        $request->execute($data);
    }
    
    return $erreur;
}

