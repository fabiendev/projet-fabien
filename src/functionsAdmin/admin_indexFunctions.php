<?php
require_once('../includes/_dispacher.php');


// Récupère les informations de l'utilisateur connecté

function userInfo() {
    global $db;

    $data['id'] = $_SESSION['id'];

    $sql = 'SELECT id, username, email, localisation, picture 
    FROM users
    WHERE id = :id';
    $request = $db->prepare($sql);
    $request->execute($data);
    $results = $request->fetchAll();

    return $results;
}

$userInfo = userInfo();


// Récupère les randonnées que l'utilisateur connecté a posté

function userHike() {
    global $db;

    $data['id'] = $_SESSION['id'];

    $sql = 'SELECT id, title, city, posted 
    FROM hikes
    WHERE id_user = :id';

    $request = $db->prepare($sql);
    $request->execute($data);
    $results = $request->fetchAll();

    return $results;
}

$userHike = userHike();


// Récupère les commentaires que l'utilisateur connecté a posté

function userComment() {
    global $db;

    $data['id'] = $_SESSION['id'];
    
    $sql = 'SELECT id, comment, posted, id_hike
    FROM comments
    WHERE id_user = :id';
    $request = $db->prepare($sql);
    $request->execute($data);
    $results = $request->fetchAll();

    return $results;
}

$userComment = userComment();





