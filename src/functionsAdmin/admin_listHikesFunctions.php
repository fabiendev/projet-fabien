<?php
require_once('../includes/_dispacher.php');

// Vérifie le rôle de l'utilisateur et le renvoi vers l'index s'il n'est pas admin
checkRole();


// Récupère toutes les randonnées de la BDD

function getHikes() {
    global $db;

    $sql = 'SELECT hikes.id, title, city, posted, id_user
    FROM hikes
    ORDER by posted';
    $request = $db->query($sql);
    $results = $request->fetchALL();

    return $results;
}

$allHikes = getHikes();