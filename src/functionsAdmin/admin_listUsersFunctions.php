<?php
require_once('../includes/_dispacher.php');

// Vérifie le rôle de l'utilisateur et le renvoi vers l'index s'il n'est pas admin
checkRole();

// Récupère tous les utilisateurs de la BDD

function getUsers() {
    global $db;

    $sql = 'SELECT id, username, email, role FROM users';
    $request = $db->query($sql);
    $results = $request->fetchALL();

    return $results;
}

$allUsers = getUsers();