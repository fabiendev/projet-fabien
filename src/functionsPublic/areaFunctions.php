<?php 
require_once('../includes/_dispacher.php');

$area = $_GET['area'];

// Transforme le raccourci en nom de la zone

function getNameArea($area) {
    if($area == 'pb') {
        $areaName = 'Pays Basque';
    } elseif($area == 'bn') {
        $areaName = 'Béarn';
    } elseif($area == 'hp') {
        $areaName = 'Hautes Pyrénées';
    } elseif($area == 'ae') {
        $areaName = 'Ariège';
    } elseif($area == 'po') {
        $areaName = 'Pyrénées Orientales';
    } else {
        $areaName = '';
    }
    return $areaName;
}

// Récupère les randonnées en fonction de la zone

function getArea($area) {
    global $db;

    $data['area'] = getNameArea($area);

    if (!empty($area)) {
        $sql = 'SELECT hikes.id, title, elevation, distance, duration, area, city, picture, name, color
        FROM hikes
        JOIN levels
        ON hikes.id_level = levels.id
        WHERE area = :area';
        $request = $db->prepare($sql);
        $request->execute($data);
        $result = $request->fetchAll();
    } else {
        $sql = 'SELECT hikes.id, title, elevation, distance, duration, area, city, picture, name, color
        FROM hikes
        JOIN levels
        ON hikes.id_level = levels.id 
        ORDER BY area';
        $request = $db->query($sql);
        $result = $request->fetchAll();
    }
    return $result;
}

// Permet de transformer le titre de la page pour qu'il s'adapte

$areaName = getNameArea($area);

function getTitle($area) {
    if (!empty($area) || $area != '') {
        $area = 'Randonnées en ' . $area;
    } else {
        $area = 'Toutes les randonnées';
    }
    return $area;
}


