<?php 
require_once('../includes/_dispacher.php');

// Fonction qui génère le lien vers une zone

function getLinkArea($area) {
    if ($area === 'pb') {
        $area = 'area.php?area=pb';
    } elseif ($area === 'bn') {
        $area = 'area.php?area=bn';
    } elseif ($area === 'hp') {
        $area = 'area.php?area=hp';
    } elseif ($area === 'ae') {
        $area = 'area.php?area=ae';
    } elseif ($area === 'po') {
        $area = 'area.php?area=po';
    } else {
        $area = 'area.php?area=';
    }
    return $area;
}

// Fonction qui compte le nombre de randonnées par zone

function countArea($area) { 
    global $db;

    $data['area'] = $area;

    $sql = 'SELECT COUNT(*)
    FROM hikes
    WHERE area = :area';
    $request = $db->prepare($sql);
    $request->execute($data);
    $areaCount = $request->fetch();
    $result = $areaCount['COUNT(*)'];

    return $result;
}
