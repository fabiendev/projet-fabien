<?php 

require_once('../includes/_dispacher.php');

// Récupère toutes les randonnée pour la page inspiration

function getInspirationHiking() {
    global $db;
    
    $sql = 'SELECT hikes.id, title, elevation, distance, duration, area, city, picture, name, color
    FROM hikes
    JOIN levels
    ON hikes.id_level = levels.id 
    ORDER BY area';
    $request = $db->query($sql);
    $result = $request->fetchAll();

    return $result;
}

$inspirationHiking = getInspirationHiking();