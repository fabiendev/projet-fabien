<?php
require_once('../includes/_dispacher.php');

// Fonction pour se logger et récupèrer le rôle, l'id et l'auth de la personne et les mets dans $_SESSION

function login() {
	global $db;
	
	if (!empty($_POST) && empty($_POST['firstname'])) {

		extract($_POST); 
	
		$error = "Les identifiants sont incorrects.";
		
		$sql = 'SELECT id, password, role FROM users WHERE email = ?';
		$request = $db->prepare($sql);
		$request->execute([$email]);
		$result = $request->fetch();
		
		if (password_verify($password, $result['password'])) {
			$_SESSION['auth'] = true;
			$_SESSION['role'] = $result['role'];
			$_SESSION['id'] = $result['id'];
			header('Location: ../pages-admin/admin_index.php');
		} else {
			return $error;
		} 	
	}
}


