<?php

require_once('../includes/_dispacher.php');


// Fonction de recherche dans la BDD suivant ce que l'utilisateur a saisi dans les moteurs de recherche sinon affiche la liste de toutes les randonnées

function searchHiking() {
    $result = [];

	if (!empty($_GET['search'])) {
        global $db;
        $data['search'] = htmlspecialchars($_GET['search']);
        $sql = "SELECT hikes.id, title, elevation, distance, duration, area, city, picture, name, color
        FROM hikes
        JOIN levels
        ON hikes.id_level = levels.id
        WHERE title LIKE '%".$data['search']."%'
        OR introduction LIKE '%".$data['search']."%'
        OR description LIKE '%".$data['search']."%'
        OR information LIKE '%".$data['search']."%'
        OR city LIKE  '%".$data['search']."%'
        OR area LIKE '%".$data['search']."%'
        ";
        $request = $db->query($sql);
        $result = $request->fetchAll();
        return $result;
    } else { 
        global $db;
        $sql = 'SELECT hikes.id, title, elevation, distance, duration, area, city, picture, name, color
        FROM hikes
        JOIN levels
        ON hikes.id_level = levels.id 
        ORDER BY area';
        $request = $db->query($sql);
        $result = $request->fetchAll();
        return $result;
    }
}



$search = $_GET['search'];
$hikingSearch = searchHiking();

function searchTitle($search, $hikingSearch) {
    if (!empty($search) && !empty($hikingSearch)) {
        $titleSearch = 'Résultats pour : "' . $_GET['search'] . '"';
    } elseif (empty($hikingSearch)) {
        $titleSearch = 'Aucun résultats pour : "' . $_GET['search'] . '"';
    } else {
        $titleSearch = 'Toutes les randonnées';
    }
    return $titleSearch;
}









