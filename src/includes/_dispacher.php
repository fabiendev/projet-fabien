<?php 

// Librairie de fonctions perso 
require_once('config.php');
require_once('database.php');
require_once('tools.php');
require_once('form.php');

// Démarrer la session
session_start();


// Vérifier si la personne est loguée pour rentrer sur les pages
checkAdmin();