<?php
// Debug pour PDO
define('DEBUG', true);

// Informations de connection à la BDD 
define('DB_HOST', 'localhost');
define('DB_NAME', 'mendia');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

// Chemins stockés dans des constantes
define('BASEPATH', '/php/001-projet');
define('ROOT', dirname(__DIR__));
define('PAGES', BASEPATH . '/src/pages-public/');
define('ASSETS', BASEPATH . '/src/assets/');
define('URL', "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
define('FILES', BASEPATH . '/src/assets/image/');
