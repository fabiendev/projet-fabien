<?php

// Améliore la lisibilité d'un var_dump
function dump($variable) {
	echo '<pre>';
	var_dump($variable);
	echo '</pre>';
}


// Fonction d'appel du header, définir public ou admin

function get_header($layout = 'public') {
	require_once ROOT . '/layouts/' . $layout . '/header.php';
}


// Fonction d'appel du footer, définir public ou admin

function get_footer($layout = 'public') {
	require_once ROOT . '/layouts/' . $layout . '/footer.php';
}


// Vérifie si la personne est loguée pour entrer sur les pages du back-office

function checkAdmin() {
	$existAdmin = strpos(URL, 'admin_');
	if ($existAdmin) {
		if (empty($_SESSION['auth'])) {
			header('Location: ' . '../pages-public/login.php');
			die();
		}
	}
}

// Verifie le rôle de l'utilisateur et le renvoi vers l'index du back-office s'il n'est pas admin

function checkRole() {
    if ($_SESSION['role'] !== 'admin') {
        header('Location: ' . 'admin_index.php');
    }
}

// Vérifie l'url et renvoie vers la page Liste utilisateur (ou admin_index si pas admin) si id est vide ou n'est pas créé

function validGetId() {
    if (!isset($_GET['id']) || $_GET['id'] == '' ) {
        header('Location: ' . 'admin_list-users.php');
    }
}


// Créé une notification
function notif(string $message, string $type = 'danger') {
	$_SESSION['notif'] = [
		'message' => $message,
		'type' => $type
	];
}

// Affiche une notification  
function displayNotif() {
	if (!empty($_SESSION['notif'])) :
		$content = '<div class="alert alert-' . $_SESSION['notif']['type'] . '" role="alert">';
		$content .= $_SESSION['notif']['message'];
		$content .= '</div>';

		unset($_SESSION['notif']);

		return $content;
	endif;
}

// Transforme les dates récupérées depuis la BDD au format français

function dateFormat($date, $time = true) {
	setlocale(LC_ALL, 'fr_FR.utf8', 'fra');
	//$format = ($time) ? ' à %H:%M:%S' : '';
	$format = '';
	if ($time) :
		$format = ' &agrave; %H:%M:%S';
	endif;
	$date = strftime('Le %A %d %B %Y' . $format, strtotime($date));

	return $date;
}


// Récupère la class couleur pour les niveaux

function getColor($level) {
    if ($level == 'Marcheur') {
        $result = 'level-green';
    } elseif ($level == 'Randonneur') {
        $result = 'level-blue';
    } else {
        $result = 'level-red';
    }
    return $result;
}

// Récupère les niveaux des randonnées en BDD

function getLevel() {
	global $db;
	$sql = 'SELECT id, name FROM levels ORDER BY name ASC';
	$request = $db->query($sql);
	return $request->fetchAll();
}


// Récupère le nom de la randonnée par rapport à l'ID envoyé

function getHikeName($id) {
    global $db;

    $data['id'] = $id;

    $sql = 'SELECT title
    FROM hikes
    WHERE id = :id';
    $request = $db->prepare($sql);
    $request->execute($data);
    $result = $request->fetch();

    return $result;
}

// Récupère le pseudo de l'utilisateur par rapport à l'ID envoyé

function getUserName($id) {
    global $db;

    $data['id'] = $id;

    $sql = 'SELECT username
    FROM users
    WHERE id = :id';
    $request = $db->prepare($sql);
    $request->execute($data);
    $result = $request->fetch();

    return $result;
}


// Récupère les randonnées recommandées (limitées à 6 en aléatoire)

function getRelatedHike() {
    global $db;

    $sql = 'SELECT hikes.id, title, introduction, city, area, elevation, duration, distance, picture, name, color
    FROM hikes
    JOIN levels
    ON hikes.id_level = levels.id
    ORDER by RAND() LIMIT 6';
    $request = $db->query($sql);
    $result = $request->fetchAll();

    return $result;
}

$relatedHike = getRelatedHike();

