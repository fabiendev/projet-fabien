'use strict'

async function getSingleMap() {
    const city = document.getElementById('city').innerText

    const localisation = await window.fetch('https://api-adresse.data.gouv.fr/search/?q=' + city + '&type=municipality')
        .then(response => response.json())
        .then(json => json.features[0].geometry.coordinates)

    const latitude = localisation[1]
    const longitude = localisation[0]
    
    let mymap = L.map('mapid').setView([latitude, longitude], 15);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 22,
        attribution: 'mendia',
        id: 'mapbox/outdoors-v11'
    }).addTo(mymap);

    L.marker([latitude, longitude]).addTo(mymap);

    mymap.scrollWheelZoom.disable();
    mymap.on('focus', () => { mymap.scrollWheelZoom.enable(); });
    mymap.on('blur', () => { mymap.scrollWheelZoom.disable(); });
}

getSingleMap()

