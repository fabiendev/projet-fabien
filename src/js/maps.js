'use strict'

function getCities(dataJson) {
    const cities = []
    for (let i = 0; i < dataJson.length; i++) {
        cities.push(dataJson[i]['city'])
    }

    return cities
}

const cities = getCities(dataJson)

async function displayAllHikes(data) {

    const cityLocalisation = []

    for (let i = 0; i < data.length; i++) {
        cityLocalisation.push(await window.fetch('https://api-adresse.data.gouv.fr/search/?q=' + data[i] + '&type=municipality')
            .then(response => response.json())
            .then(json => json))
    }

    const localisation = []

    for (let i = 0; i < cityLocalisation.length; i++) {
        localisation.push(cityLocalisation[i]['features'][0]['geometry']['coordinates'])
    }

    let mymap = L.map('mapid').setView([42.878085, 0.645245], 8);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 22,
        attribution: 'Mendia © OpenStreetMap contributors, Imagery © Mapbox',
        id: 'mapbox/outdoors-v11'
    }).addTo(mymap);

    for (let i = 0; i < localisation.length; i++) {
        let marker = L.marker([localisation[i][1], localisation[i][0]]);
        marker.bindPopup("<strong>" + dataJson[i].title + "</strong><br><img src=\"../assets/image/hiking/" + dataJson[i].picture + "\" alt=\"mountain\"><a href=\"../pages-public/single.php?id=" + dataJson[i].id + "\"> Voir la randonnée");
        marker.addTo(mymap);
    }

    mymap.scrollWheelZoom.disable();
    mymap.on('focus', () => { mymap.scrollWheelZoom.enable(); });
    mymap.on('blur', () => { mymap.scrollWheelZoom.disable(); });
}

displayAllHikes(cities)