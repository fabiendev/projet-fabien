</main>

<footer class="footer">
    <div class="footer__container flex-column">
        <div class="footer__logo">
            <a class="footer__logo-link" href="../../src/pages-public/index.php">
                <img src="../../src/assets/image/logo/mountain.png" alt="logo" class="footer__logo-picture">
            </a>
        </div>
        <nav class="footer__nav">
            <ul class="footer__menu flex-column">
                <li class="footer__menu-item"><a href="../../src/pages-public/index.php" class="footer__menu-link">Accueil</a></li>
                <li class="footer__menu-item"><a href="../../src/pages-public/index.php" class="footer__menu-link">Mentions légales</a></li>
                <li class="footer__menu-item"><a href="mailto:mendia@protonmail.com" class="footer__menu-link">Contact</a></li> 
                
            </ul>
        </nav>
        <div class="footer__copyright">
            <span>Mendia ©2020-2021</span>
        </div>
    </div>
</footer>
</body>
</html>