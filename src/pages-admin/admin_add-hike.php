<?php
require_once('../functionsAdmin/admin_addHikeFunctions.php');
get_header('admin');
?>

<section class="container p-3 mb-3">
	<div class="row mb-4 g-0">
		<h1 class="col-6 fw-bold">Ajouter une randonnée</h1>
		<div class="col-6 text-end">
			<a href="admin_index.php" class="btn btn-primary" title="Dashboard">Retour</a>
		</div>
	</div>
	<div class="container g-0">
		<p class="fw-bold text-primary">* Champs requis</p>
		<div class="container">
			<?php if(!empty($_POST)) {
				$erreur = addHike();
				if(isset($erreur)) {
					if($erreur) {
						foreach($erreur as $value) { ?>
						<div class="error_notif">
							<div class="alert alert-danger" role="alert"><?php echo $value; ?></div>
						</div>
						<?php } 
					} else { ?>
						<div class="confirmation_notif">
							<div class="alert alert-success" role="alert">La randonnée à bien été ajoutée</div>
						</div>
					<?php }
				} 
			} ?>
		</div>
		<form action="" method="post" class="form" novalidate autocomplete="off" enctype="multipart/form-data">
			<div class="form-group">
				<label for="title">Titre de la randonnée*</label>
				<input type="text" class="form-control mt-2" value="<?php if(isset($_POST["title"])) echo $_POST["title"] ?>" id="title" name="title">
			</div>
			<div class="form-group mt-3">
				<label for="introduction">Introduction*</label>
				<textarea name="introduction" class="form-control mt-2" id="introduction"><?php if(isset($_POST["introduction"])) echo $_POST["introduction"] ?></textarea>
				<small id="roleHelp" class="form-text text-muted">Petit paragraphe accrocheur pour présenter votre randonnée (maxi 800 caractères).</small>
			</div>
			<div class="form-group mt-3">
				<label for="description">Description*</label>
				<textarea name="description" class="form-control mt-2" id="description"><?php if(isset($_POST["description"])) echo $_POST["description"] ?></textarea>
				<small id="roleHelp" class="form-text text-muted">Description de la randonnée, chemins à suivre, changements de direction...</small>
			</div>
			<div class="form-group mt-3">
				<label for="information">Information</label>
				<textarea name="information" class="form-control mt-2" id="information"><?php if(isset($_POST["information"])) echo $_POST["information"] ?></textarea>
				<small id="roleHelp" class="form-text text-muted">Indiquez par exemple les points d'eau, abris, l'équipement nécessaire, conseils de sécurités....</small>
			</div>
			<div class="form-group mt-3">
				<label for="area">Zone géographique*</label>
				<select class="form-control mt-2" name="area" id="area">
					<option value="">Sélectionner une zone</option>
						<option value="Pays Basque">Pays Basque</option>
						<option value="Béarn">Béarn</option>
						<option value="Hautes Pyrénées">Hautes Pyrénées</option>
						<option value="Ariège">Ariège</option>
						<option value="Pyrénées Orientales">Pyrénées Orientales</option>
				</select>
			</div>
			<div class="form-group mt-3">
				<label for="city">Ville de départ*</label>
				<input type="text" class="form-control mt-2" value="<?php if(isset($_POST["city"])) echo $_POST["city"] ?>" id="city" name="city">
			</div>
			<div class="form-group mt-3">
				<label for="level">Niveau*</label>
				<select class="form-control mt-2" name="id_level" id="id_level">
					<option value="">Difficulté</option>
					<?php foreach (getLevel() as $value) { ?>
						<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group mt-3">
				<label for="elevation">Dénivelé</label>
				<input type="text" class="form-control mt-2" value="<?php if(isset($_POST["elevation"])) echo $_POST["elevation"] ?>" id="elevation" name="elevation">
				<small id="roleHelp" class="form-text text-muted">Renseignez le dénivelé approximatif sous cette forme, ex : 780m ou 1200m</small>
			</div>
			<div class="form-group mt-3">
				<label for="duration">Durée*</label>
				<input type="text" class="form-control mt-2" value="<?php if(isset($_POST["duration"])) echo $_POST["duration"] ?>" id="duration" name="duration">
				<small id="roleHelp" class="form-text text-muted">Renseignez la durée approximative comprenant les pauses sous cette forme, ex : 30min ou 4h30</small>

			</div>
			<div class="form-group mt-3">
				<label for="distance">Distance</label>
				<input type="text" class="form-control mt-2" value="<?php if(isset($_POST["distance"])) echo $_POST["distance"] ?>" id="distance" name="distance">
				<small id="roleHelp" class="form-text text-muted">Renseignez la distance approximative sous cette forme, ex : 3km ou 11,50km</small>
			</div>
			<div class="form-group mt-3">
				<label for="picture">Image*</label>
				<input class="form-control-file mt-2" type="file" id="picture" name="picture">
			</div>
			<div>
				<input type="hidden" name="id_user" value="<?= $_SESSION['id']; ?>">
			</div>
			<div class="form-group mt-4">
				<input type="submit" value="Sauvegarder" class="btn btn-primary">
			</div>
		</form>
	</div>
</section>

<?php get_footer('admin'); ?>
