<?php
require_once('../functionsAdmin/admin_deleteUserFunctions.php');
get_header('admin'); 
?>

<section class="container bg-light p-3 mt-5 mb-3">
    <h1 class="fw-bold text-center mt-5">Supprimer le compte: "<?php echo $currentUser['username']; ?>"</h1>
    <h5 class="text-center mt-4">Êtes vous certain de vouloir supprimer définitivement cet utilisateur ?</h5>
    <div class="row mt-5 mb-5 g-0">
        <div class="col-12 text-center">
            <a href="admin_delete-user.php?id=<?php echo $_GET['id']; ?>&confirm=1" title="Supprimer définitivement" class="btn btn-danger btn-lg me-3">
                Supprimer
            </a>
            <a href="admin_index.php" title="Annuler la suppression" class="btn btn-secondary btn-lg">
                Annuler
            </a>
        </div>
    </div>
</section>

<?php get_footer('admin'); ?>