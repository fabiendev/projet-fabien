<?php
require_once('../functionsAdmin/admin_listHikesFunctions.php');
get_header('admin');
?>

<section class="container p-3 mb-3">
	<div class="row mb-4 g-0">
		<h1 class="col-6 fw-bold">Toutes les randonnées</h1>
		<div class="col-6 text-end">
			<a href="admin_add-hike.php" class="btn btn-success" title="Ajouter">Ajouter</a>
			<a href="admin_index.php" class="btn btn-primary" title="Dashboard">Retour</a>
		</div>
	</div>
	<div class="container g-0">
		<div class="table-responsive-md">
			<table class="table table-striped table-hover">
				<thead class="primary-color text-white text-center">
					<tr class="d-flex">
						<th class="col">Titre</th>
						<th class="col">Ville</th>
						<th class="col">Posté</th>
						<th class="col">Auteur</th>
						<th class="col-4 col-md-3">Actions</th>
					</tr>
				</thead>
				<tbody class="text-center">
					<?php foreach ($allHikes as $value) { ?>
						<tr class="d-flex">
							<td class="col align-middle"><?php echo $value['title']; ?></td>
							<td class="col align-middle"><?php echo $value['city']; ?></td>
							<td class="col align-middle"><?php echo dateFormat($value['posted'], false); ?></td>
							<td class="col align-middle"><?php echo getUserName($value['id_user'])['username'];; ?></td>
							<td class="col-4 col-md-3 align-middle">
								<div class="d-flex flex-column flex-md-row justify-content-md-center">
									<a href="../pages-public/single.php?id=<?php echo $value['id']; ?>" class="btn btn-primary btn-sm me-md-1" title="Voir">Voir</a>
									<a href="admin_edit-hike.php?id=<?php echo $value['id']; ?>" class="btn btn-warning btn-sm me-md-1 mt-1 mt-md-0" title="Modifier">Modifier</a>					
									<a href="admin_delete-hike.php?id=<?php echo $value['id']; ?>" class="btn btn-danger btn-sm mt-1 mt-md-0" title="Supprimer">Supprimer</a>					
								</div>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</section>


<?php get_footer('admin'); ?>