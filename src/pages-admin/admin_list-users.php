<?php
require_once('../functionsAdmin/admin_listUsersFunctions.php');
get_header('admin'); 
?>

<section class="container p-3 mb-3">
	<div class="row mb-4 g-0">
		<h1 class="col-6 fw-bold">Liste des utilisateurs</h1>
		<div class="col-6 text-end">
			<a href="admin_add-user.php" class="btn btn-success" title="Ajouter">Ajouter</a>
			<a href="admin_index.php" class="btn btn-primary" title="Dashboard">Retour</a>
		</div>
	</div>
	<div class="container g-0">
		<div class="table-responsive-md">
			<table class="table table-striped table-hover">
				<thead class="primary-color text-white text-center">
					<tr class="d-flex">
						<th class="col">Pseudo</th>
						<th class="col">Email</th>
						<th class="col">Rôle</th>
						<th class="col-5 col-md-4">Actions</th>
					</tr>
				</thead>
				<tbody class="text-center">
					<?php foreach ($allUsers as $value) { ?>
						<tr class="d-flex">
							<td class="col align-middle"><?php echo $value['username']; ?></td>
							<td class="col align-middle"><?php echo $value['email']; ?></td>
							<td class="col align-middle">
								<?php if ($value['role'] == 'admin') {
									echo 'Administrateur';
								} else {
									echo 'Utilisateur';
								} ?>
							</td>
							<td class="col-5 col-md-4 align-middle">
								<div class="d-flex flex-column flex-md-row justify-content-md-center">
									<a href="admin_edit-user.php?id=<?php echo $value['id']; ?>" class="btn btn-warning btn-sm me-md-1" title="Modifier">Modifier</a>
									<a href="admin_delete-user.php?id=<?php echo $value['id']; ?>" class="btn btn-danger btn-sm mt-1 mt-md-0" title="Supprimer">Supprimer</a>
								</div>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</section>


<?php get_footer('admin'); ?>