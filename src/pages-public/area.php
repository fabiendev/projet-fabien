<?php  
require_once('../functionsPublic/areaFunctions.php');
get_header('public');
?>

<section class="area">
    <div class="area__head">
        <h1 class="area__head-title title title--big"><?php echo getTitle($areaName); ?></h1>
    </div>
    <div class="area__container container container--full flex-row hike">
        <?php foreach (getArea($area) as $value) { ?>
            <article class="hike__item flex-column">
                <div class="hike__head">
                    <a href="single.php?id=<?php echo $value['id'] ?>" class="hike__head-link">
                        <img src="../assets/image/hiking/<?php echo $value['picture'] ?>" class="hike__head-picture" alt="Randonnée">
                    </a>
                    <div class="hike__head-box <?php echo getColor($value['name']); ?>">
                        <span class="hike__head-level">Niveau: <?php echo $value['name']; ?></span>
                    </div>
                </div>
                <h3 class="hike__title title title--small"><?php echo $value['title']; ?></h3>
                <div class="hike__detail flex-row flex-row--center">
                    <div class="hike__detail-left flex-column">
                        <i class="hike__detail-icon fas fa-map-signs"></i>
                        <span class="hike__detail-area"><?php echo $value['area']; ?></span>
                        <span class="hike__detail-city"><?php echo $value['city']; ?></span>
                    </div>
                    <div class="hike__detail-right flex-column">
                        <i class="hike__detail-icon fas fa-hiking"></i>
                        <span class="hike__detail-elevation">Dénivelé: <?php echo $value['elevation']; ?></span>
                        <span class="hike__detail-duration"><?php echo $value['duration'] . ' - ' . $value['distance']; ?></span>
                    </div>
                </div>
            </article>
        <?php } ?>
    </div>
</section>

<?php get_footer('public'); ?>