<?php 
require_once('../functionsPublic/guideFunctions.php');
get_header('public'); 
?>
<section class="guide">
    <div class="guide__head">
        <h1 class="guide__head-title title title--big">Répertoire des randonnées</h1>
    </div>
    <div class="guide__container container container--full flex-row flex-row--start">
        <div class="guide__card">
            <a href="<?php echo getLinkArea('pb'); ?>" class="guide__card-link"><img src="../assets/image/guide/pays_basque.jpg" alt="Pays Basque" class="guide__card-picture"></a>
            <div class="guide__card-content">
                <h3 class="guide__card-area title title--small">Pays Basque</h3>
                <span class="guide__card-number"><?php echo countArea('Pays Basque'); ?> Randonnées</span>
            </div>
        </div>
        <div class="guide__card">
            <a href="<?php echo getLinkArea('bn'); ?>" class="guide__card-link"><img src="../assets/image/guide/béarn.jpg" alt="Béarn" class="guide__card-picture"></a>
            <div class="guide__card-content">
                <h3 class="guide__card-area title title--small">Béarn</h3>
                <span class="guide__card-number"><?php echo countArea('Pays Basque'); ?> Randonnées</span>
            </div>
        </div>
        <div class="guide__card">
            <a href="<?php echo getLinkArea('hp'); ?>" class="guide__card-link"><img src="../assets/image/guide/hautes_pyrénées.jpg" alt="Hautes Pyrénées" class="guide__card-picture"></a>
            <div class="guide__card-content">
                <h3 class="guide__card-area title title--small">Hautes Pyrénées</h3>
                <span class="guide__card-number"><?php echo countArea('Hautes Pyrénées'); ?> Randonnées</span>
            </div>
        </div>
        <div class="guide__card">
            <a href="<?php echo getLinkArea('ae'); ?>" class="guide__card-link"><img src="../assets/image/guide/ariège.jpg" alt="Ariège" class="guide__card-picture"></a>
            <div class="guide__card-content">
                <h3 class="guide__card-area title title--small">Ariège</h3>
                <span class="guide__card-number"><?php echo countArea('Ariège'); ?> Randonnées</span>
            </div>
        </div>
        <div class="guide__card">
            <a href="<?php echo getLinkArea('po'); ?>" class="guide__card-link"><img src="../assets/image/guide/pyrénées_orientales.jpg" alt="Pyrénées Orientales" class="guide__card-picture"></a>
            <div class="guide__card-content">
                <h3 class="guide__card-area title title--small">Pyrénées Orientales</h3>
                <span class="guide__card-number"><?php echo countArea('Pyrénées Orientales'); ?> Randonnées</span>
            </div>
        </div>
    </div>
</section>


<?php get_footer('public'); ?>