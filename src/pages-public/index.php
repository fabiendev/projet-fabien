<?php
require_once('../functionsPublic/indexFunctions.php');
get_header('public');
?>

<section class="banner">
    <div class="search flex-column">
        <h1 class="search__title title title--big">Trouver votre randonnée</h1>
        <form action="search.php" autocomplete="off" method="get" class="search__form flex-row flex-row--center">
            <input class="search__form-input" type="text" placeholder="Lieux, nom de la randonnée, niveau..." name="search" value="<?php echo searchValue(); ?>"/>
            <button class="search__form-submit" type="submit"><i class="fas fa-search"></i></button>  
        </form>
    </div>
    <div class="scroll">
        <a href="#discover-anchor"><img src="../assets/image/scroll2.png" alt="Scroll" class="scroll__icon"></a>
    </div>
</section>

<section class="discover" id="discover-anchor">
    <div class="discover__container container container--full">
        <h2 class="discover__title title title title--medium">Découvrir</h2>
        <div class="discover__hike flex-row hike">
            <?php foreach ($relatedHike as $value) { ?>
                <article class="hike__item flex-column">
                    <div class="hike__head">
                        <a href="single.php?id=<?php echo $value['id'] ?>" class="hike__head-link">
                            <img src="../assets/image/hiking/<?php echo $value['picture'] ?>" class="hike__head-picture" alt="Randonnée">
                        </a>
                        <div class="hike__head-box <?php echo getColor($value['name']); ?>">
                            <span class="hike__head-level">Niveau: <?php echo $value['name']; ?></span>
                        </div>
                    </div>
                    <h3 class="hike__title title title--small"><?php echo $value['title']; ?></h3>
                    <div class="hike__detail flex-row flex-row--center">
                        <div class="hike__detail-left flex-column">
                            <i class="hike__detail-icon fas fa-map-signs"></i>
                            <span class="hike__detail-area"><?php echo $value['area']; ?></span>
                            <span class="hike__detail-city"><?php echo $value['city']; ?></span>
                        </div>
                        <div class="hike__detail-right flex-column">
                            <i class="hike__detail-icon fas fa-hiking"></i>
                            <span class="hike__detail-elevation">Dénivelé: <?php echo $value['elevation']; ?></span>
                            <span class="hike__detail-duration"><?php echo $value['duration'] . ' - ' . $value['distance']; ?></span>
                        </div>
                    </div>
                </article>
            <?php } ?>
        </div>
        <div class="discover__more">
            <a href="area.php?area=" class="discover__more-btn btn btn--small">Voir toutes les randonnées</a>
        </div>
    </div>
</section>
<section class="cta">
    <div class="cta__container container container--full flex-column">
        <div class="cta__card">
            <img src="../assets/image/card/icon_hike.png" alt="Visuel randonnée" class="cta__card-picture">
            <h3 class="cta__card-title title title--small">Trouvez des randonnées</h3>
            <div class="cta__card-more">
                <a class="cta__card-more-btn btn btn--small" href="inspiration.php">Parcourir</a>
            </div>
        </div>
        <div class="cta__card">
            <img src="../assets/image/card/icon_map.png" alt="Visuel carte" class="cta__card-picture">
            <h3 class="cta__card-title title title--small">Partager vos randonnées</h3>
            <div class="cta__card-more">
                <a class="cta__card-more-btn btn btn--small" href="login.php">Ajouter</a>
            </div>
        </div>
        <div class="cta__card">
            <img src="../assets/image/card/icon_mountain.png" alt="Visuel montagnes" class="cta__card-picture">
            <h3 class="cta__card-title title title--small">Sécurité en montagne</h3>
            <div class="cta__card-more">
                <a class="cta__card-more-btn btn btn--small" href="safety.php">Consulter</a>
            </div>
        </div>
    </div>
    <div class="scroll">
        <a href="#global-map-anchor"><img src="../assets/image/scroll2.png" alt="Scroll" class="scroll__icon"></a>
    </div>
</section>

<section class="global-map" id="global-map-anchor">
    <div class="global-map__container container container--full">
        <h2 class="global-map__title title title--medium">Carte des randonnées</h2>
        <div id="mapid" class="global-map__map"></div>
    </div>
</section>

<script>
    let dataJson = <?php echo $hikeJson ?>
</script>

<script src="../js/maps.js"></script>

<?php get_footer('public'); ?>
