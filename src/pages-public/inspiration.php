<?php 
require_once('../functionsPublic/inspirationFunctions.php');
get_header('public'); 
?>

<section class="inspiration">
    <div class="inspiration__head">
        <h1 class="inspiration__head-title title title--big">Filtrez les randonnées par zones</h1>
        <div id="filterBtn" class="inspiration__filter flex-row flex-row--evenly">
            <button class="inspiration__filter-btn btn active" onclick="filterSelection('all')">Voir tout</button>
            <button class="inspiration__filter-btn btn" onclick="filterSelection('Pays Basque')">Pays Basque</button>
            <button class="inspiration__filter-btn btn" onclick="filterSelection('Béarn')">Béarn</button>
            <button class="inspiration__filter-btn btn" onclick="filterSelection('Hautes Pyrénées')">Hautes Pyrénées</button>
            <button class="inspiration__filter-btn btn" onclick="filterSelection('Ariège')">Ariège</button>
            <button class="inspiration__filter-btn btn" onclick="filterSelection('Pyrénées Orientales')">Pyrénées Orientales</button>
        </div>
    </div>
    <div class="inspiration__container container container--full flex-row hike">
        <?php foreach ($inspirationHiking as $value) { ?>
            <article class="hike__item flex-column filterDiv <?php echo $value['area']; ?>">
                <div class="hike__head">
                    <a href="single.php?id=<?php echo $value['id'] ?>" class="hike__head-link">
                        <img src="../assets/image/hiking/<?php echo $value['picture'] ?>" class="hike__head-picture" alt="Randonnée">
                    </a>
                    <div class="hike__head-box <?php echo getColor($value['name']); ?>">
                        <span class="hike__head-level">Niveau: <?php echo $value['name']; ?></span>
                    </div>
                </div>
                <h3 class="hike__title title title--small"><?php echo $value['title']; ?></h3>
                <div class="hike__detail flex-row flex-row--center">
                    <div class="hike__detail-left flex-column">
                        <i class="hike__detail-icon fas fa-map-signs"></i>
                        <span class="hike__detail-area"><?php echo $value['area']; ?></span>
                        <span class="hike__detail-city"><?php echo $value['city']; ?></span>
                    </div>
                    <div class="hike__detail-right flex-column">
                        <i class="hike__detail-icon fas fa-hiking"></i>
                        <span class="hike__detail-elevation">Dénivelé: <?php echo $value['elevation']; ?></span>
                        <span class="hike__detail-duration"><?php echo $value['duration'] . ' - ' . $value['distance']; ?></span>
                    </div>
                </div>
            </article>
        <?php } ?> 
    </div>
</section>

<!-- Filtre article page inspiration -->
<script src="../../src/js/articleFilter.js"></script>


<?php get_footer('public'); ?>