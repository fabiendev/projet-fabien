<?php 
require_once('../functionsPublic/loginFunctions.php'); 
get_header('login');
?>


<section class="login">
	<div class="login__container">
		<div class="login__logo">
			<a class="login__logo-link" href="index.php">
				<img src="../../src/assets/image/logo/mountain.png" alt="logo" class="login__logo-picture">
			</a>
		</div>
		<div class="login__head">
			<h1 class="login__head-title title title--small">Espace personnel</h1>
			<span class="login__head-subtitle"><a href="login.php" class="login__head-link">Connectez-vous</a>  ou <a href="register.php" class="login__head-link">créez un compte</a> pour accéder à votre espace personnel.</p>
		</div>
		<div class="login__main">
			<h2 class="login__main-title title title--small">Se connecter</h2>
			<form action="" method="post" class="login__form">
				<div class="login__form-group">
					<label for="email" class="sr-only">Email</label>
					<input type="email" name="email" placeholder="Adresse email" id="email" value="<?php if(isset($_POST["email"])) echo $_POST["email"] ?>"  class="login__form-input" autofocus>
				</div>
				<div class="login__form-group">
					<label for="password" class="sr-only">Mot de passe</label>
					<input type="password" name="password" placeholder="Mot de passe" id="password" class="login__form-input">
				</div>
				<div class="login__form-group login__form-firstname">
					<label for="firstname" class="sr-only">Votre prénom</label>
					<input type="text" name="firstname" id="firstname" class="login__form-input">
				</div>
				<?php if(!empty($_POST)) {
					$error = login();
					if(isset($error)) { ?>
					<div class="login__form-notif error_notif">
						<span class="error_message"><?php echo $error; ?></span>
					</div>
					<?php }
				} ?>
				<button type="submit" class="login__form-btn btn">Se connecter</button>
			</form>
			<div class="login__forgot">
				<a href="" title="Mot de passe oublié" class="login__forgot-link">Mot de passe oublié ?</a>
			</div>
			<div class="login__register">
				<h2 class="login__register-subtitle">Vous n'avez pas encore de compte ?</h2>
				<a href="register.php" class="login__register-link" title="S'enregistrer">Enregistrer-vous</a>
			</div>
		</div>
	</div>
</section>
	  
<?php get_footer('login'); ?>

