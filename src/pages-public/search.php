<?php
require_once('../functionsPublic/searchFunctions.php');
get_header('public');
?>

<section class="search-head">
    <div class="search-head__container">
        <h1 class="search-head__title title title--big"><?php echo searchTitle($search, $hikingSearch); ?></h1>
    </div>
    <div class="search-head__search">
        <h2 class="search-head__search-title title title--small">Nouvelle recherche</h2>
        <form class="search-head__search-form flex-row flex-row--center" action="search.php" autocomplete="off" method="get">
            <input class="search-head__search-input" type="text" placeholder="Rechercher une randonnée" name="search" value="<?php echo searchValue(); ?>"/>
            <button class="search-head__search-submit" type="submit"><i class="fas fa-search"></i></button>    
        </form>
    </div>
</section>
<?php if (!empty($hikingSearch)) { ?>
    <section class="search-result flex-row hike">
        <div class="search-result__hike flex-row hike">
            <?php foreach ($hikingSearch as $value) { ?>
                <article class="hike__item flex-column">
                    <div class="hike__head">
                        <a href="single.php?id=<?php echo $value['id'] ?>" class="hike__head-link">
                            <img src="../assets/image/hiking/<?php echo $value['picture'] ?>" class="hike__head-picture" alt="Randonnée">
                        </a>
                        <div class="hike__head-box <?php echo getColor($value['name']); ?>">
                            <span class="hike__head-level">Niveau: <?php echo $value['name']; ?></span>
                        </div>
                    </div>
                    <h3 class="hike__title title title--small"><?php echo $value['title']; ?></h3>
                    <div class="hike__detail flex-row flex-row--center">
                        <div class="hike__detail-left flex-column">
                            <i class="hike__detail-icon fas fa-map-signs"></i>
                            <span class="hike__detail-area"><?php echo $value['area']; ?></span>
                            <span class="hike__detail-city"><?php echo $value['city']; ?></span>
                        </div>
                        <div class="hike__detail-right flex-column">
                            <i class="hike__detail-icon fas fa-hiking"></i>
                            <span class="hike__detail-elevation">Dénivelé: <?php echo $value['elevation']; ?></span>
                            <span class="hike__detail-duration"><?php echo $value['duration'] . ' - ' . $value['distance']; ?></span>
                        </div>
                    </div>
                </article>
            <?php } ?>
        </div>
    </section>
<?php } else {  ?>
    <section class="search-lack">
        <h2 class="search-lack__title title title--medium">Randonnées qui peuvent vous intéresser</h2>
        <div class="search-lack__hike flex-row hike">
            <?php foreach ($relatedHike as $value) { ?>
                <article class="hike__item flex-column">
                    <div class="hike__head">
                        <a href="single.php?id=<?php echo $value['id'] ?>" class="hike__head-link">
                            <img src="../assets/image/hiking/<?php echo $value['picture'] ?>" class="hike__head-picture" alt="Randonnée">
                        </a>
                        <div class="hike__head-box <?php echo getColor($value['name']); ?>">
                            <span class="hike__head-level">Niveau: <?php echo $value['name']; ?></span>
                        </div>
                    </div>
                    <h3 class="hike__title title title--small"><?php echo $value['title']; ?></h3>
                    <div class="hike__detail flex-row flex-row--center">
                        <div class="hike__detail-left flex-column">
                            <i class="hike__detail-icon fas fa-map-signs"></i>
                            <span class="hike__detail-area"><?php echo $value['area']; ?></span>
                            <span class="hike__detail-city"><?php echo $value['city']; ?></span>
                        </div>
                        <div class="hike__detail-right flex-column">
                            <i class="hike__detail-icon fas fa-hiking"></i>
                            <span class="hike__detail-elevation">Dénivelé: <?php echo $value['elevation']; ?></span>
                            <span class="hike__detail-duration"><?php echo $value['duration'] . ' - ' . $value['distance']; ?></span>
                        </div>
                    </div>
                </article>
            <?php } ?>
        </div>
        <div class="search-lack__more">
            <a href="area.php?area=" class="search-lack__more-btn btn btn--small">Voir toutes les randonnées</a>
        </div>
    </section>
<?php } ?>


<?php get_footer('public'); ?>