<?php 
require_once('../functionsPublic/singleFunctions.php');
get_header('public');
?>

<?php foreach ($singleHike as $value) { ?>
    <section class="single-head flex-column">
            <span class="single-head__title-container">
                <h1 class="single-head__title title title--big"><?php echo $value['title']; ?></h1>
            </span>
            <img src="../assets/image/hiking/<?php echo $value['picture'] ?>" alt="<?php echo $value['title']; ?>" class="single-head__image">
            <div class="single-head__details flex-row flex-row--between">
                <div class="single-head__details-item">
                    <h3 class="single-head__details-title title">Secteur</h3>
                    <span class="single-head__details-content"><?php echo $value['area']; ?></span>
                </div>
                <div class="single-head__details-item">
                    <h3 class="single-head__details-title title">Dénivelé</h3>
                    <span class="single-head__details-content"><?php echo $value['elevation']; ?></span>
                </div>
                <div class="single-head__details-item">
                    <h3 class="single-head__details-title title">Durée</h3>
                    <span class="single-head__details-content"><?php echo $value['duration']; ?></div>
                <div class="single-head__details-item">
                    <h3 class="single-head__details-title title">Distance</h3>
                    <span class="single-head__details-content"><?php echo $value['distance']; ?></span>
                </div>
                <div class="single-head__details-item <?php echo getColor($value['name']); ?>">
                    <h3 class="single-head__details-title title">Niveau</h3>
                    <span class="single-head__details-content"><?php echo ucfirst($value['name']); ?></span>
                </div>
                <div class="single-head__details-item">
                    <h3 class="single-head__details-title title">Départ</h3>
                    <span id="city" class="single-head__details-content"><?php echo $value['city']; ?></span>
                </div>
                <div class="single-head__details-item">
                    <h3 class="single-head__details-title title">Auteur</h3>
                    <span class="single-head__details-content"><?php echo $value['username']; ?></span>
                </div>
            </div>
        
    </section>

    <div class="single-container flex-row">
        <div class="single-container__content flex-column">
            <section class="single-content">
                <div class="single-content__container">
                    <div id="mapid" class="single-content__container-map"></div>
                </div>
                <article class="single-content__item">
                    <div class="single-content__box">
                        <h2 class="single-content__box-title title title--small">Introduction:</h2> 
                        <p class="single-content__box-text"><?php echo nl2br($value['introduction']); ?></p>
                    </div>
                    <div class="single-content__box">
                        <h2 class="single-content__box-title title title--small">Description:</h2>
                        <p class="single-content__box-text"><?php echo nl2br($value['description']); ?></p>
                    </div>
                    <div class="single-content__box">
                        <h2 class="single-content__box-title title title--small">Information:</h2>
                        <p class="single-content__box-text"><?php echo nl2br($value['information']); ?></p>
                    </div>
                </article>
            </section>
<?php } ?>
            <section class="comment">
                <div class="comment__post">
                    <span class="comment__post-title" id="comment">Poster un commentaire</span>
                    <div class="comment__post-container">
                        <?php if(isset($_SESSION["id"])) { 
                            if (!empty($_POST)) { ?>
                                <div class="comment__post-notif">
                                    <?php echo $notif ?>
                                </div>
                            <?php } ?>
                            <form method="post" action="#comment" class="comment__post-form">
                                <textarea class="comment__post-input" name="comment" placeholder="Votre commentaire..."></textarea>
                                <input class="comment__post-submit btn" type="submit" value="Commenter">
                            </form>
                        <?php } else { ?>
                            <div class="comment__post-connection">
                                <p>Vous devez être <a href="login.php">connecter</a> pour pourvoir commenter.</p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="comment__read" x-data="{ open: false }">
                    <span class="comment__read-btn btn" @click="open = true">Voir les <?php echo $nbComments['COUNT(*)'] ?> commentaires</span>
                    <div class="comment__read-dropdown" x-show="open" @click.away="open = false">
                        <?php foreach($comments as $comment) {?>
                            <div class="comment__read-item">
                                <?php if (!empty($comment['picture'])) { ?>
                                    <img class="comment__read-avatar" src="../assets/image/user/<?php echo $comment['picture']; ?>" alt="avatar">
                                <?php } else { ?>
                                    <img class="comment__read-avatar" src="../assets/image/user/default.png" alt="avatar">
                                <?php } ?>
                                <div class="comment__read-body">
                                    <div class="comment__read-head">
                                        <span class="comment__read-user">Posté par <?php echo $comment["username"]; ?> - </span>
                                        <span class="comment__read-date"><?php echo dateFormat($comment['posted']); ?></span>
                                    </div>
                                    <div class="comment__read-text">
                                        <p class="comment__read-message"><?php echo nl2br($comment["comment"]); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </div>
    
        <aside class="single-container__sidebar">
            <section class="sidebar">
                <div class="sidebar__container">
                    <div class="sidebar__weather">
                        <div class="weather">
                            <div class="weather__head">
                                <?php foreach ($singleHike as $value) { ?>
                                    <h2 class="weather__head-title">Météo à</h2> 
                                    <span class="weather__head-city" id="city"><?php echo $value['city']; ?></span>
                                <?php } ?>
                            </div>
                            <div class="weather__container flex-column">
                                <div id="weather-j0">
                                    <h3 class="title title--small">Aujourd'hui</h3>
                                    <i class="j0 wi"></i>
                                    <div class="condition j0"></div>
                                    <div class="temp-min">
                                        Température min : <span class="min-j0"></span>C°
                                    </div>
                                    <div class="temp-max">
                                        Température max : <span class="max-j0"></span>C°
                                    </div>
                                </div>
                                <div id="weather-j1">
                                    <h3 class="title title--small">J+1</h3>
                                    <i class="j1 wi"></i>
                                    <div class="condition j1"></div>
                                    <div class="temp-min">
                                        Température min : <span class="min-j1"></span>C°
                                    </div>
                                    <div class="temp-max">
                                        Température max : <span class="max-j1"></span>C°
                                    </div>
                                </div>
                                <div id="weather-j2">
                                    <h3 class="title title--small">J+2</h3>
                                    <i class="j2 wi"></i>
                                    <div class="condition j2"></div>
                                    <div class="temp-min">
                                        Température min : <span class="min-j2"></span>C°
                                    </div>
                                    <div class="temp-max">
                                        Température max : <span class="max-j2"></span>C°
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar__documentation">
                        <div class="sidebar__documentation-head">
                            <h3 class="sidebar__documentation-title">Documentation</h3>
                        </div>
                        <a href="safety.php" class="sidebar__documentation-link"><img src="../assets/image/safety/single-safety.jpg" alt="safety" class="sidebar__documentation-picture"></a>
                    </div>
                </div>
            </section>
        </aside>
    </div>
        


<script src="../js/meteo.js"></script>
<script src="../js/mapSingle.js"></script>

<?php get_footer('public'); ?>